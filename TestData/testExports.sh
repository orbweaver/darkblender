#!/bin/sh
blender -b Cube.blend --python-expr "import bpy; bpy.ops.object.select_all(action='SELECT'); bpy.ops.export_scene.ase(filepath='Cube.ase')"
blender -b AutoSmooth.blend --python-expr "import bpy; bpy.ops.object.select_all(action='SELECT'); bpy.ops.export_scene.lwo(filepath='AutoSmooth.lwo')"
blender -b 3Cubes.blend --python-expr "import bpy; bpy.ops.object.select_all(action='SELECT'); bpy.ops.export_scene.ase(filepath='3Cubes.ase')"
