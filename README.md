## Blender resources for The Dark Mod

### Working scripts

This repository contains the following usable scripts:

* `tdm_export.py`: export script for ASE and LWO model formats
* `io_import_lwo.py`: import script for LWO model format

To install the scripts, simply download them to a location of your choice, then
install them via the `Add-ons` section of Blender preferences. The new file
formats should appear under `Import` or `Export` in the `File` menu.

**NOTE**: Make sure to download the scripts from the correct branch. The
`latest` branch has scripts tested with the most recent official release of
Blender (there is no testing with unreleased alpha or beta versions). Branches
with names like `blender-2.79` contain versions of the scripts that work(ed)
with the specified version of Blender, and _might_ work with later versions if
there are no API incompatibilities. Unless you specifically need compatibility
with an older Blender version, use the `latest` branch.

#### Avoiding Blender material name limitation

Blender currently has a limitation of 63 characters for material names, which is
sometimes insufficient for the longer material names used in The Dark Mod. In
order to work around this problem, the `tdm_export` script supports a custom
parameter `FullName`, which can be set on a material in Blender using the
**Custom properties** panel.

If a material has a `FullName` custom property set, the value of this property
will be used to name the exported material in the ASE or LWO file (the
Blender-visible material name will be ignored). Custom property values do not
suffer from the same 63-character limitation as material names, which enables
the use of the full set of Dark Mod materials.

### Partially working scripts

The repository also contains the following scripts which do not yet work
completely:

* `io_import_ase.py`: import script for ASE model format (imports geometry only
— no materials, UVs or vertex colors).

### Application template

In the Template directory is a zipped startup file which can be installed as a
separate application template (available from the splash screen or from the
**File -> New** submenu). This sets up an empty scene with certain settings that
are more appropriate for creating models for the Dark Mod, such as grid lines
every 8 units (rather than 10), and a far clip plane that is far enough away
not to clip objects that are several thousand units in size.

In order to install the template:

- From Blender's *Application menu* (click on the Blender icon at the top
  left), choose `Install Application Template...`
- Navigate and choose the `DarkMod.zip` archive.
- The template will now be installed (and copied into your own user profile
  directory), and there should now be an option `Dark Mod modelling` in the
  **File -> New** submenu).
