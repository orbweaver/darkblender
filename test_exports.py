import unittest
import sys
import os
import filecmp
from pathlib import Path
from tempfile import TemporaryDirectory

import bpy  # type:ignore


def testdata_dir() -> Path:
    return Path(os.path.dirname(os.path.realpath(__file__))) / "TestData"


class ExporterTest(unittest.TestCase):

    def do_export_test(self, file_basename: str, format: str):
        """Test an export to the given format from the given input file."""

        blend_file = file_basename + ".blend"
        reference_file = file_basename + "." + format

        with self.subTest(blend_file=blend_file, reference_file=reference_file):
            # Load cube file
            blend_path = testdata_dir() / blend_file
            bpy.ops.wm.open_mainfile(filepath=str(blend_path))
            bpy.ops.object.select_all(action="SELECT")

            # Export to a temporary directory
            with TemporaryDirectory() as temp_dir:
                exported_file = f"{temp_dir}/export.{format}"
                export_method = getattr(bpy.ops.export_scene, format)
                export_method(filepath=exported_file)

                # Confirm that the exported file matches expectations
                expected_file = testdata_dir() / reference_file
                self.assertTrue(filecmp.cmp(expected_file, exported_file))

    def test_export_ase(self):
        """Run all ASE export tests."""

        self.do_export_test("Cube", "ase")
        self.do_export_test("3Cubes", "ase")

    def test_export_lwo(self):
        """Run all LWO export tests."""

        self.do_export_test("AutoSmooth", "lwo")


if __name__ == "__main__":
    argv = [__file__] + (
        sys.argv[sys.argv.index("--") + 1 :] if "--" in sys.argv else []
    )
    unittest.main(argv=argv)
